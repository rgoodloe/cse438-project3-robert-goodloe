In this file you should include:

Any information you think we should know about your submission
* Is there anything that doesn't work? Why?
		- If you go to the leaderboard activity and come back it deals you a new hand rather than picking up where you left off
		- GameActivity.kt and Game.kt were an attempt to refactor my code. They are extremely close to working, but I did not have time to finish.
			They are not integrated with any files currently.
		- If a user runs out of money there is no way to get more
* Is there anything that you did that you feel might be unclear? Explain it here.
		- Gestures: 
				swipe right: hit
				double tap: stand
				swipe up: bet


A description of the creative portion of the assignment
* Describe your feature
	I implemented the ability to place bets.
* Why did you choose this feature?
	I think that it is an important part of the game of blackjack
* How did you implement it?
	I created a new vector image asset with which to display the chip. I added a swipe up gesture that creates a chip image view and animates it so that it
	"throws the chip into a pile". Each chip is worth $50. When the user wins they are rewarded 2x their bet, losing removes their bet amount from their money, 
	and a draw reqults in no money lost or gained. There is an ante of $50 placed at the beginning of each game. If the dealer wins immediately with a 21, the ante is lost.
	There is a maximum bet of $500, and the user may not bet after they hit or stand. The users start with $1000. 

(10 points) Player can login and login data is stored in Firebase
(10 points) Player’s win/loss counts are displayed on startup
(10 points) The Player receives two cards face up, and the dealer receives one card face up and one card face down
(5 points) Swiping right allows the Player to hit
(5 points) Double tapping allows the Player to stand
(10 points) Cards being dealt are animated, and all cards are visible.
(5 points) If the Player goes over 21, they automatically bust
(10 points) The dealer behaves appropriately based on the rules described above
(10 points) Once the game is complete, the winner should be declared and the Firebase database should be updated appropriately
(5 points) A new round is automatically started after each round
(5 points) Player can logout
(10 points) A leaderboard is shown in a separate tab or activity and is consistent among installations of the app
(15 points) Creative portion!

I'm actually not very upset about the fact that it doesn't resume the current hand when you go back from the leaderboard. It is pretty minor. I really like the way you implemented betting, especially with the chip graphics, that was a nice touch.

I didn't see any problems with your app at all, I think you did a great job.

Total: 110 / 110
